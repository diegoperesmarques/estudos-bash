#!/usr/bin/env bash

# Variáveis globais
num_escolhido=0
resulatdo=0

#Funções

_titulo() {
	clear
	for i in {1..20};do
		echo -n "#"
	done
	echo ""
	echo $1
	echo ""
	for i in {1..20};do
		echo -n "#"
	done
	echo ""
}

_opcoesMenu() {
	echo "1. Somar"
	echo "2. Subtrair"
	echo "3. Multiplicar"
	echo "4. Finalizar Programa"

	read -p "Escolha uma opção acima: " num_escolhido
}

_validacaoMenu() {
	case $1 in
		1) 
			_somar ;;
		2)
			_subtrair ;; 
		3) 
			_multiplicar ;;
		*) 
		read -p "Opção inválida! Tente novamente!"
	esac

}

_somar() {
	_titulo Somando
	
	read -p "Digite o primeiro número: " numero1
	read -p "Digite o segundo número: " numero2
	
	echo "O resultado da soma é $(( $numero1 + $numero2 ))" 
	read continua

}

_subtrair() {
	_titulo Subtraindo
	
	read -p "Digite o primeiro número: " numero1
	read -p "Digite o segundo número: " numero2
	
	echo "O resultado da subtração é $(( $numero1-$numero2 ))" 
	read continua
}

_multiplicar() {
	_titulo Multiplicando
	
	read -p "Digite o primeiro número: " numero1
	read -p "Digite o segundo número: " numero2
	
	echo "O resultado da multiplicação é $(( $numero1*$numero2))" 
	read continua
}

while true;do
	_titulo "Calculador 1.0"
	_opcoesMenu
	
	[[ $num_escolhido -eq 4 ]] && echo "Saindo ... " && break
	
	_validacaoMenu $num_escolhido
	
done
